#include "stdint.h"

typedef struct{
    float fInCoef;
    float fR_n, fR_n1, fR_n2;
    float fA1, fA2, fB1;
    float fInX, fOutX;
}IIR_Sections_t;
 
typedef struct{
    float fOut;
    IIR_Sections_t *ptrSection;
    uint16_t u16SectionLen;
}IIR_Filter_t;
 
float ExecuteIIR(IIR_Filter_t *Obj, float in);
