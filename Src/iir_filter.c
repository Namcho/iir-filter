#include "iir_filter.h"

/*
* @brief         : Matlab fdatool kullanilarak yapilan IIR filtre yapisi referans alinarak  yazilmistir.
* @param         Obj: IIR filtre bilesenlerinin tutuldugu structure
* @param		 input: Giris sinyalinin anlik degeri
* @return        : Yok
* @precondition  : Yok
* @postcondition : Yok
*/
float ExecuteIIR(IIR_Filter_t *Obj, float input){
    uint16_t i;
    IIR_Sections_t *sectionStartADR;
    
    //Baslangic adresini kaydedelim
    sectionStartADR = Obj->ptrSection;
    //Sinyali section1'in girisine atayalim
    Obj->ptrSection->fInX = input;
  
    for(i=0; i<Obj->u16SectionLen; i++){
        //Rn'i hesaplayalim
        Obj->ptrSection->fR_n = Obj->ptrSection->fInX * Obj->ptrSection->fInCoef;
        Obj->ptrSection->fR_n -= Obj->ptrSection->fR_n1 * Obj->ptrSection->fA1;
        Obj->ptrSection->fR_n -= Obj->ptrSection->fR_n2 * Obj->ptrSection->fA2;
        
        //Cikisi hesaplayalim
        Obj->ptrSection->fOutX = Obj->ptrSection->fR_n;
        Obj->ptrSection->fOutX += Obj->ptrSection->fR_n1 * Obj->ptrSection->fB1;
        Obj->ptrSection->fOutX += Obj->ptrSection->fR_n2;  
        
        //Ornek gecitirme islemleri
        Obj->ptrSection->fR_n2 = Obj->ptrSection->fR_n1;
        Obj->ptrSection->fR_n1 = Obj->ptrSection->fR_n;
        if(i == 2){
            Obj->ptrSection->fR_n2 = 0.0f;
        }
        //Bir sonraki section'a gecelim
        Obj->ptrSection++;
        Obj->ptrSection->fInX = (sectionStartADR + i)->fOutX; 
    }
    //Sonuncu section'da elde edilen cikis bilgisini alalim
    Obj->fOut = (Obj->ptrSection-1)->fOutX;
    Obj->ptrSection = sectionStartADR;
 
    return Obj->fOut;
}

